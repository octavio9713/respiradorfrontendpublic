import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginformComponent} from "./pages/login/loginform/loginform.component";
import {AuthGuard} from "./security/guard/auth.guard";


const routes: Routes = [
  {path: 'login',  component: LoginformComponent},
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'pages', canActivate: [AuthGuard], loadChildren: () =>
      import('./pages/pages.module').then(mod => mod.PagesModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
