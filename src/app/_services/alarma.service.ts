import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Alerta} from "../_models/Alerta";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AlarmaService {

  public resourceUrl = environment.SERVER_API_URL + 'api/alertas';

  constructor(private http: HttpClient) { }

  findAllAlarms(): Observable<Alerta[]> {
    return this.http.get<Alerta[]>(this.resourceUrl );
  }
}
