import {Injectable, OnInit} from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Alerta} from "../../_models/Alerta";
import {BehaviorSubject, Observable} from "rxjs";
import {ValoresNormales} from "../../_models/valores-normales";
import {Paciente} from "../../_models/paciente";

@Injectable({
  providedIn: 'root'
})
export class ConfigService implements OnInit{

  public resourceUrl = environment.SERVER_API_URL + 'api/paciente';
  public configurUrl = environment.SERVER_API_URL + 'api/valores-normales';

  configs: ValoresNormales;
  viewedConfigs: BehaviorSubject<ValoresNormales> = new BehaviorSubject<ValoresNormales>(this.configs);

  constructor(private http: HttpClient) {
    this.http.get<Paciente>(this.resourceUrl + '/' + localStorage.getItem(environment.tokenName))
      .subscribe(patient => {
        this.configs = patient.valoresNormalesDTO;
      });
  }

  ngOnInit(): void {

  }

  updateConfig(config: ValoresNormales): Observable<ValoresNormales> {
    this.configs = config;
    this.viewedConfigs.next(this.configs);
    return this.http.put<ValoresNormales>(this.configurUrl, config);
  }

}
