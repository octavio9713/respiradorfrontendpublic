import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {PeriodoDeAtencion} from "../../_models/periodo-de-atencion";
import {Paciente} from "../../_models/paciente";

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  public resourceUrl = environment.SERVER_API_URL + 'api/paciente';

  constructor(private http: HttpClient) { }

  getPatient(number: string): Observable<Paciente>{
    return this.http.get<Paciente>(this.resourceUrl + '/' + number);
  }
}
