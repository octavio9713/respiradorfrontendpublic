import { Injectable } from '@angular/core';
import {PeriodoDeAtencion} from "../_models/periodo-de-atencion";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class PeriodoService {

  public resourceUrl = environment.SERVER_API_URL + 'api/periodo';
  public paciente = environment.SERVER_API_URL + 'api/paciente';

  constructor(private http: HttpClient) { }

  savePatientVitals(periodoAtencion: PeriodoDeAtencion): Observable<PeriodoDeAtencion> {
    return this.http.put<PeriodoDeAtencion>(this.resourceUrl, periodoAtencion);
  }

  getPeriodos(number): Observable<PeriodoDeAtencion> {
    return this.http.get<PeriodoDeAtencion>(this.paciente + '/periodos/' + number);
  }
}
