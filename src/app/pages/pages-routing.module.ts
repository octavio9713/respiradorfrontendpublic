import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PagesComponent} from "./pages.component";


const routes: Routes =[{

  path: '',
  component: PagesComponent,
  children: [
    {
      path: '',
      redirectTo: 'patient',
      pathMatch: 'full'
    },
    {
      path: 'patient',
      loadChildren: () => import('./patient/patient.module').then(mod => mod.PatientModule)
    },
    // {
    //   path: 'dashboard',
    //   loadChildren: "",
    // },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
