import { Component, OnInit } from '@angular/core';
import {ConfigService} from "../../_services/confing/config.service";
import {Alerta} from "../../_models/Alerta";
import {ValoresNormales} from "../../_models/valores-normales";

@Component({
  selector: 'app-alert-modal',
  templateUrl: './alert-modal.component.html',
  styleUrls: ['./alert-modal.component.css']
})
export class AlertModalComponent implements OnInit {

  config: ValoresNormales;

  constructor(private configService: ConfigService) {
    this.config = configService.configs;
  }

  ngOnInit() {
  }

  changeValues(){
    this.configService.updateConfig(this.config).subscribe();
  }

}
