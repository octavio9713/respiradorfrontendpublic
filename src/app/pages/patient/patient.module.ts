import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';

import { PatientRoutingModule } from './patient-routing.module';
import { VitalSignsComponent } from './components/vital-signs/vital-signs.component';
import { PatientInfoComponent } from './components/patient-info/patient-info.component';
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatGridListModule} from "@angular/material/grid-list";
import {ChartCommonModule, LineChartModule} from "@swimlane/ngx-charts";
import {MatIconModule} from "@angular/material/icon";
import {MatDividerModule} from "@angular/material/divider";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatChipsModule} from "@angular/material/chips";
import { HistoricComponent } from './components/historic/historic.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatListModule} from "@angular/material/list";
import {} from "@angular/core/"


@NgModule({
  declarations: [VitalSignsComponent, PatientInfoComponent, HistoricComponent],
  imports: [
    CommonModule,
    PatientRoutingModule,
    MatSidenavModule,
    MatGridListModule,
    LineChartModule,
    ChartCommonModule,
    MatIconModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatChipsModule,
    MatDialogModule,
    MatSnackBarModule,
    MatListModule
  ],
  providers: [DatePipe],
  exports: [VitalSignsComponent]
})
export class PatientModule { }
