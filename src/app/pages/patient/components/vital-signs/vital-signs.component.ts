import {Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import * as shape from 'd3-shape';
import {PatientService} from "../../../../_services/patient/patient.service";
import {interval, Subscription} from "rxjs";
import {IMqttMessage, MqttService} from "ngx-mqtt";
import {ConfigService} from "../../../../_services/confing/config.service";
import {SignosVitales} from "../../../../_models/signos-vitales";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ValoresNormales} from "../../../../_models/valores-normales";
import {PeriodoDeAtencion} from "../../../../_models/periodo-de-atencion";
import {Paciente} from "../../../../_models/paciente";
import {mergeMap, switchMap, tap} from "rxjs/operators";
import {PeriodoService} from "../../../../_services/periodo.service";
import {AlarmaService} from "../../../../_services/alarma.service";
import {Alerta} from "../../../../_models/Alerta";
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-vital-signs',
  templateUrl: './vital-signs.component.html',
  styleUrls: ['./vital-signs.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class VitalSignsComponent implements OnInit, OnDestroy {

  curve: any = shape.curveCardinal;

  heartBeats = [{
    name: "Beats per Minute",
    series: []
  }];
  actualBeatValue;
  heartBeatColor = {
    domain: ['#e42323']
  };

  oxigenPM = [{
    name: "Oxigen in Blood",
    series: []
  }];
  actualSpO2Value;
  oxigenColor = {
    domain: ['#2fdee4']
  };

  heartSubscription: Subscription;
  heartIndex = 0;
  heartTopic = "heartRate";

  acutalBreathValue;
  breathSubscription: Subscription;
  breathIndex = 0;
  breathTopic = "breathPerMinute";

  oxigenSubscription: Subscription;
  oxigenIndex = 0;
  oxigenTopic = "Spo2";

  presionSitolica;
  presionSitolicaSubscription: Subscription;
  presionSitolicaTopic = "presionSistolica";
  presionSistolicaIndex = 0;

  presionDiastolica;
  presionDiastolicaSubscription: Subscription;
  presionDiastolicaTopic = "presionDistolica";
  presionDiastolicaIndex = 0;

  temperaturaActual;
  temperaturaSubscription: Subscription;
  temperaturaTopic = "temperatura";
  temperaturaIndex = 0;

  alertSubscription: Subscription;
  alertTopic = "alertVitals";

  configAlarm: ValoresNormales;

  vitals: SignosVitales[] = [];

  patient: Paciente;

  @ViewChild("vitals", {static: false})
  component: any;

  //Normal time 300000
  saveInterval = interval(30000);

  periodoActual: PeriodoDeAtencion;

  alarmWorking = false;

  alertas: Alerta[] = [];

  constructor(private patientService: PatientService,
              private periodoService: PeriodoService,
              private alarmService: AlarmaService,
              private _mqttService: MqttService,
              private confService: ConfigService,
              private snackBar: MatSnackBar,
              public datepipe: DatePipe) {

    this.patientService.getPatient("1")
      .pipe(
        tap(patient => {
          this.patient = patient;
          this.configAlarm = patient.valoresNormalesDTO;
        }),
        switchMap(patient => {
          return this.periodoService.getPeriodos(patient.numeroPaciente);
      })).subscribe(periodos => {
        this.periodoActual = periodos[0];
        this.periodoActual.pacienteDTO = this.patient;
      });

    this.confService.viewedConfigs.subscribe(conf=>{
      this.configAlarm = conf;
    });

    this.alarmService.findAllAlarms().subscribe(alerts => {
      this.alertas = alerts;
    })

  }

  ngOnInit() {
    this.subscribeToHeart();

    this.subscribeToBreathing();

    this.subscribeToOxigen();

    this.subscribeToPressure();

    this.subscribeToTemperature();

    //Happens when an alert has sent generated
    this.alertSubscription = this._mqttService.observe(this.alertTopic).subscribe((message: IMqttMessage) =>
      this.generateAlert(message.payload.toString())
    );

    //Every x minutes saves the ongoing values
    this.saveInterval.subscribe(() => this.saveVitals() );
  }

  ngOnDestroy(): void {
    this.heartSubscription.unsubscribe();
    this.breathSubscription.unsubscribe();
    this.oxigenSubscription.unsubscribe();
    this.alertSubscription.unsubscribe();
  }

  /*########################## Subscriptions START: ##########################*/

  private subscribeToHeart() {

    this.heartSubscription = this._mqttService
      .observe(this.heartTopic).subscribe((message: IMqttMessage) => {

        let alerta = null;

        const value = +message.payload.toString();
        if (value < this.configAlarm.pulsoMinimo || value > this.configAlarm.pulsoMaximo)
          alerta = this.sendAlert("Pulso", value > this.configAlarm.pulsoMaximo);


        this.heartBeats[0].series.push(
          {
            "name": this.heartIndex,
            "value": +message.payload.toString()
          }
        );

        if (this.heartBeats[0].series.length > 35){
          this.heartBeats[0].series.splice(0, 1);
        }

        this.heartBeats = [{
          name: this.heartBeats[0].name,
          series: this.heartBeats[0].series
        }];
        this.actualBeatValue = +message.payload.toString();

        this.createVital(this.heartIndex);

        this.vitals[this.heartIndex].pulso = +message.payload.toString();

        if (alerta != null)
          this.vitals[this.heartIndex].alertas.push(alerta);

        this.heartIndex++;

      });
  }

  private subscribeToBreathing() {
    this.breathSubscription = this._mqttService
      .observe(this.breathTopic).subscribe((message: IMqttMessage) => {

        let alerta = null;

        const value = +message.payload.toString();

        if (value < this.configAlarm.respiracionMinima || value > this.configAlarm.respiracionMaxima)
          alerta = this.sendAlert("Respiracion", value > this.configAlarm.respiracionMaxima);

        this.acutalBreathValue = +message.payload.toString();

        this.createVital(this.breathIndex);

        this.vitals[this.breathIndex].respiracion = +message.payload.toString();
        if (alerta != null)
          this.vitals[this.breathIndex].alertas.push(alerta);

        this.breathIndex++;

      });
  }

  private subscribeToOxigen() {
    this.oxigenSubscription = this._mqttService
      .observe(this.oxigenTopic).subscribe((message: IMqttMessage) => {

        const value = +message.payload.toString();

        let alerta = null;
        if (value < this.configAlarm.oxigenoEnSangreMinimo || value > this.configAlarm.oxigenoEnSangreMaximo)
          alerta = this.sendAlert("Oxigeno En Sangre", value > this.configAlarm.oxigenoEnSangreMaximo);

        this.oxigenPM[0].series.push(
          {
            "name": this.oxigenIndex,
            "value": +message.payload.toString()
          }
        );

        if (this.oxigenPM[0].series.length > 35){
          this.oxigenPM[0].series.splice(0, 1);
        }

        this.oxigenPM = [{
          name: this.oxigenPM[0].name,
          series: this.oxigenPM[0].series
        }];
        this.actualSpO2Value = +message.payload.toString();

        this.createVital(this.oxigenIndex);

        this.vitals[this.oxigenIndex].oxigenoEnSangre = +message.payload.toString();
        if (alerta != null)
          this.vitals[this.oxigenIndex].alertas.push(alerta);

        this.oxigenIndex++;

      });
  }

  private subscribeToPressure() {

    this.presionDiastolicaSubscription = this._mqttService
      .observe(this.presionDiastolicaTopic).subscribe((message: IMqttMessage) =>{

        const value = +message.payload.toString();

        let alerta = null;
        if (value < this.configAlarm.presionDiastolicaMinima || value > this.configAlarm.presionDiastolicaMaxima)
          alerta = this.sendAlert("Presion Diastolica", value > this.configAlarm.presionDiastolicaMaxima);

        this.presionDiastolica = +message.payload.toString();

        this.createVital(this.presionDiastolicaIndex);

        if (alerta != null)
          this.vitals[this.presionDiastolicaIndex].alertas.push(alerta);

        this.vitals[this.presionDiastolicaIndex].presionDiastolica = +message.payload.toString();
        this.presionDiastolicaIndex++;
      });

    this.presionSitolicaSubscription = this._mqttService
      .observe(this.presionSitolicaTopic).subscribe((message: IMqttMessage) =>{

        const value = +message.payload.toString();

        let alerta = null;
        if (value < this.configAlarm.presionSistolicaMinima || value > this.configAlarm.presionSistolicaMaxima)
          alerta = this.sendAlert("presion Sistolica", value > this.configAlarm.presionSistolicaMaxima);

        this.presionSitolica = +message.payload.toString();

        this.createVital(this.presionSistolicaIndex);

        if (alerta != null)
          this.vitals[this.presionSistolicaIndex].alertas.push(alerta);

        this.vitals[this.presionSistolicaIndex].presionSistolica = +message.payload.toString();
        this.presionSistolicaIndex++;
      });
  }

  private subscribeToTemperature() {

    this.temperaturaSubscription = this._mqttService
      .observe(this.temperaturaTopic).subscribe((message: IMqttMessage) =>{

        const value = +message.payload.toString();

        let alerta = null;
        if (value < this.configAlarm.temperaturaMinima || value > this.configAlarm.temperaturaMaxima)
          alerta = this.sendAlert("Temperatura", value > this.configAlarm.temperaturaMaxima);

        this.temperaturaActual = +message.payload.toString();

        this.createVital(this.temperaturaIndex);
        if (alerta != null)
          this.vitals[this.presionSistolicaIndex].alertas.push(alerta);

        this.vitals[this.temperaturaIndex].temperatura = +message.payload.toString();
        this.temperaturaIndex++;
      });
  }

  /*########################## Subscriptions END: ##########################*/

  /*########################## Usefull Methods ##########################*/

  private createVital(index) {
    if (!this.vitals[index]) {
      this.vitals[index] = new SignosVitales("");
      this.vitals[index].fechaObtencion = this.datepipe.transform(new Date(), 'dd-MM-yyyy');
      this.vitals[index].horaObtencion = this.datepipe.transform(new Date(), 'hh:mm:ss');
    }

  }

  //LimitBreak means that it has gone over the limit
  private sendAlert(sign: string, limitBreak: boolean): Alerta {

    let alert;
    let message;
    let problem;
    if (limitBreak) {
      alert = this.alertas.find(a => a.signo == sign && a.mensajeDeAlerta.includes("Supero"));
      message = "El signo " + sign + " Supero el maximo";
      problem = "Supera el maximo";
    }

    else {
      alert = this.alertas.find(a => a.signo == sign && a.mensajeDeAlerta.includes("debajo"));
      message = "El signo " + sign + " esta por debajo de los limites recomendados";
      problem = "No alcanza el minimo";
    }


    this._mqttService.unsafePublish(this.alertTopic, message, {qos: 1, retain: true});
    return alert;
  }

  private saveVitals(){
    this.periodoActual.signosVitales = this.periodoActual.signosVitales.concat(this.vitals);
    this.periodoService.savePatientVitals(this.periodoActual).subscribe();
    this.vitals = [];
    this.alertas = [];
  }

  private generateAlert(message: string) {
    if (this.actualHasValue() && !this.alarmWorking) {
      this.alarmWorking = true;
      this.component._element.nativeElement.classList.add("laser");
      this.playAlarmSound();
      this.snackBar.open(message, 'Cerrar', {
        horizontalPosition: "right",
        verticalPosition: "top",
        duration: 5000,
      });

      setTimeout(() => {
        this.component._element.nativeElement.classList.remove("laser");
        this.alarmWorking = false;
      }, 10500);
    }
  }

  playAlarmSound() {
    // let audio = new Audio();
    // audio.src = "/assets/audio/siren_sound.mp3";
    // audio.load();
    // audio.play();
  }

  private actualHasValue(): boolean {
    return this.actualSpO2Value || this.acutalBreathValue || this.actualSpO2Value ||
      this.temperaturaActual || this.presionDiastolica || this.presionDiastolica
  }

}
