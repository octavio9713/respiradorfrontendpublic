import {Component, Input, OnInit} from '@angular/core';
import {Paciente} from "../../../../_models/paciente";

@Component({
  selector: 'patient-info',
  templateUrl: './patient-info.component.html',
  styleUrls: ['./patient-info.component.css']
})
export class PatientInfoComponent implements OnInit {


  @Input()
  get patient(): Paciente{
    return this._patient;
  }

  set patient(val){
    this._patient = new Paciente(val);
  }

  _patient: Paciente;

  constructor() { }

  ngOnInit() {
  }

}
