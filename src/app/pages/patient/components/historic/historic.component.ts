import { Component, OnInit } from '@angular/core';
import {PatientService} from "../../../../_services/patient/patient.service";
import {Paciente} from "../../../../_models/paciente";

@Component({
  selector: 'app-historic',
  templateUrl: './historic.component.html',
  styleUrls: ['./historic.component.css']
})
export class HistoricComponent implements OnInit {

  patient: Paciente;

  constructor(patientService: PatientService) {
    patientService.getPatient("1").subscribe(data => {
      this.patient = data;
    });
  }
  ngOnInit() {
  }

}
