import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {VitalSignsComponent} from "./components/vital-signs/vital-signs.component";

const routes: Routes = [
  {path: 'vitals', component: VitalSignsComponent},
  {path: '', redirectTo: 'vitals', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientRoutingModule { }
