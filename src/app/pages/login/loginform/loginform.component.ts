import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthenticationService} from "../../../security/service/authentication.service";

@Component({
  selector: 'app-loginform',
  templateUrl: './loginform.component.html',
  styleUrls: ['./loginform.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginformComponent implements OnInit {
  loginForm = this.fb.group({
    username: [null, Validators.required],
  });

  constructor(public fb: FormBuilder,
              private service: AuthenticationService,
              private router: Router) { }

  ngOnInit() {
    if (this.service.isLogedIn())
      this.router.navigate(["pages"]);

  }

  login() {
    this.service.login(this.loginForm.value.username).subscribe(res => {
        console.log(res);
        this.router.navigate(["pages"]);
      })
  }

  public errorUsername() {
    return this.loginForm.controls.username.invalid &&
      (this.loginForm.controls.username.dirty || this.loginForm.controls.username.touched);
  }

}
