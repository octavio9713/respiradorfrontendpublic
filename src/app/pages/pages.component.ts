import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../security/service/authentication.service";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {AlertModalComponent} from "./alert-modal/alert-modal.component";

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css']
})
export class PagesComponent implements OnInit {

  constructor(private authService: AuthenticationService,
              private dialog: MatDialog) { }

  ngOnInit() {
  }

  logout(){
    this.authService.logout();
  }

  openModal(){
    const dialogRef = this.dialog.open(AlertModalComponent);
  }

}
