export class Alerta {

  signo: string;
  motivo: string;
  mensajeDeAlerta: string;

  constructor(values: any) {
    Object.assign(this, values);
  }
}
