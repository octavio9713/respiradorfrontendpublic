export class ValoresNormales {

  id: number;
  presionSistolicaMinima: number;
  presionSistolicaMaxima: number;
  presionDiastolicaMinima: number;
  presionDiastolicaMaxima: number;
  pulsoMinimo: number;
  pulsoMaximo: number;
  oxigenoEnSangreMinimo: number;
  oxigenoEnSangreMaximo: number;
  respiracionMinima: number;
  respiracionMaxima: number;
  temperaturaMinima: number;
  temperaturaMaxima: number;

  constructor(value: any) {
    Object.assign(this, value);
  }

}
