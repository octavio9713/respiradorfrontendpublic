import {SignosVitales} from "./signos-vitales";
import {Paciente} from "./paciente";

export class PeriodoDeAtencion {

  id: number;
  fechaInicio: Date;
  fechaFin: Date;
  pacienteDTO: Paciente;

  signosVitales: SignosVitales[] = [];

  constructor(values: any) {
    Object.assign(this, values);
  }
}
