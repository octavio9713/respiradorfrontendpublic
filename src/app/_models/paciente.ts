import {ValoresNormales} from "./valores-normales";
import {PeriodoDeAtencion} from "./periodo-de-atencion";

export class Paciente {

  id: number;
  numeroPaciente: number;
  nombres:string;
  apellidos: string;
  fechaNacimiento: Date;
  genero: string;
  edad: number;

  periodosDeAtencion: PeriodoDeAtencion[] = [];
  valoresNormalesDTO: ValoresNormales;


  constructor(value: any) {
    Object.assign(this, value);

    var parts = value.fechaNacimiento.split("-");
    this.fechaNacimiento = new Date(parseInt(parts[2]), parseInt(parts[1]), parseInt(parts[0]));

    this.edad = new Date().getFullYear() - this.fechaNacimiento.getFullYear();
  }

}
