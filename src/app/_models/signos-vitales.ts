import {Alerta} from "./Alerta";

export class SignosVitales {

  id: number;

  fechaObtencion: string;
  horaObtencion: string;

  pulso: number;
  respiracion: number;
  oxigenoEnSangre: number;

  temperatura: number;

  presionSistolica: number;
  presionDiastolica: number;

  alertas: Alerta[] = [];

  constructor(values: any){
    Object.assign(this, values);
  }
}
