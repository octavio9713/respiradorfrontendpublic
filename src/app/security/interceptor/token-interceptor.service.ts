import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {AuthenticationService} from "../service/authentication.service";
import {Router} from "@angular/router";
import {Observable, of} from "rxjs";

@Injectable()
export class TokenInterceptor implements HttpInterceptor{

  constructor(private auth: AuthenticationService, private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (!this.auth.isLogedIn()) {
      this.router.navigate(["login"]);
      return of(null);
    }

    req = req.clone({
      setHeaders: {
        "Authorization": this.auth.token
      }
    });

    return next.handle(req);
  }

}
