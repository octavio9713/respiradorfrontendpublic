import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {environment} from "../../../environments/environment";
import {tap} from "rxjs/operators";
import {Paciente} from "../../_models/paciente";

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {

  public resourceUrl = environment.SERVER_API_URL + 'api/paciente';

  private _loggedIn : BehaviorSubject<boolean> =  new BehaviorSubject(false);

  constructor(private http: HttpClient,
              private router: Router) { }

  get loggedIn(): Observable<boolean>{
    return this._loggedIn.asObservable();
  }

  isLogedIn(): boolean {
    const token = localStorage.getItem(environment.tokenName);
    return token != null;
  }

  login(number: string): Observable<Paciente>{
    return this.http.get<Paciente>(this.resourceUrl + '/' + number).pipe(tap(res => {
      localStorage.setItem(environment.tokenName, "Token");
      localStorage.setItem(environment.tokenName, res.numeroPaciente.toString());
      this._loggedIn.next(true);
    }));
  }

  logout() {
    this._loggedIn.next(false);
    localStorage.removeItem(environment.tokenName);
    return this.router.navigate(['login']);
  }

  get token(): string {
    return localStorage.getItem(environment.tokenName);
  }
}
