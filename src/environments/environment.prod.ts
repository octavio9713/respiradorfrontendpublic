export const environment = {
  production: true,
  hostname: "localhost",
  port: "9001",
  tokenName: "TOKEN",
  patientId: "",
  SERVER_API_URL: "http://localhost:9010/"
};
